---
title: 'Vincent Purcell'
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: asc
    limit: 50
    pagination: false
---

## UI/UX Engineer &amp; Full Stack Designer

I'm passionate about technology and design, bridging the world of design and development, actively working as an interface designer, user researcher, media producer, fabricator and frontend developer. Formerly, I founded and ran a [makerspace in East Baltimore](/portfolio/boomspace) as part of a [workforce development collaborative](https://bniajfi.org/currentprojects/bcji/), worked as a multimedia designer and front end development lead at [emocha Mobile Health](https://www.emocha.com/), and am currently the Senior UI/UX Engineer at Baltimore based educational software company [No, Inc](http://www.noinc.com/).

### More about my work

Impact Design Hub (Contributing Editor)
[What The Maker Movement Needs To Learn](https://impactdesignhub.org/2015/11/18/what-the-maker-movement-needs-to-learn/) (Article Author, November 2015)

Open Society Institute-Baltimore Community
[Fellowship Profile](https://www.osibaltimore.org/author/vincentpurcell/) (Fellowship, 2014-2016)

Warnock Foundation Baltimore Social Innovation
[Fellowship Profile](http://warnockfoundation.org/portfolio_page/vincent-purcell/) and [Radio Interview](http://wypr.org/post/baltimores-future-vincent-purcell) (Fellow, Spring 2015)

### Selected Projects
