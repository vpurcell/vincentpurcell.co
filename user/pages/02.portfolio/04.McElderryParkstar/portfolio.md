---
title: 'McElderry Park Star'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
---

# McElderry Park Star
## Connecting a community through communication

===

The McElderry Park Star is a newspaper made for and by the residents of McElderry Park in East Baltimore. The newspaper is published every two months, and is produced by a team of youth designers who are paid through advertising revenue from local businesses.

Read about it on <a href="https://medium.com/@jadedoto/the-future-of-media-in-baltimore-6ea5162fa6fc#.rpqwhofns">Medium</a>
