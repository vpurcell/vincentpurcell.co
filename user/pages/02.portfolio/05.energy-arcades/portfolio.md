---
title: 'Energy Arcades'
published: true
---
# Energy Arcades
## Engaging audiences around energy efficiency

The Energy Arcade is a custom-fabricated set of 5 interactive games designed to teach about energy efficiency in an engaging way. Originally prototyped in a collabration funded by the University of Maryland Center for Environmental Science as part of the Maryland Department of the Environment's "Empowers" program, the second-generation arcade is now used by the Montgomery County government.

The Energy Arcade is a fresh take on teaching citizens of all ages how they can be more energy efficient at home. The games are presented with information about energy saving measures that save money and are good for the environment. The five games are:

1. Energy Hogs - About the usage of efficient appliances and alternatives to energy hogs in the home
2. Whac-A-Pollutant - A "Whac a Mole" type game encouraging changing your air filter regularly to make HVAC systems more efficient and the air we breathe healthier
3. Energy Racers - An engaging two-plater game demonstrating the incredible efficiency of LED lighting over conventional incandescent bulbs
4. House of Holes - A hand-sewn 6'x6'x4' inflatable house with holes and velco bandages demonstrating the places where homes most often leak warm/cool air when using HVAC systems
5. Water Usage - A simple game demonstrating how much water we really use when taking a long shower
