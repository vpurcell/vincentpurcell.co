---
title: 'Newspaper Redesign Concept'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# Baltimore Sun Newspaper Concept
## Concept, design and physical mockup for a redesign of the Baltimore Sun Newspaper.

===

<div class="col-xs-12 embed">
<h3>Design Concept</h3>
<iframe style="width:100%; height:500px;" src="//e.issuu.com/embed.html#10295212/6104778" frameborder="0" allowfullscreen></iframe>
</div>
