---
title: 'PrEPme by emocha'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# PrEPme by emocha.
## Strategic design, frontend development and mobile interface development.

===

Increasing access to PrEP, a once-daily pill that prevents HIV.

The emocha platform integrates patient engagement tools that coordinate care from diagnosis through treatment. emocha’s mobile application allows patients to connect with health navigators who can answer questions and schedule appointments with local providers. Providers are able to track their patients’ progress on a web interface, while patients have access to resources, appointment reminders, and the ability to chat with providers from their mobile app in real time. Among other use cases, emocha has successfully linked patients to pre-exposure prophylaxis (PrEP) to prevent the transmission of HIV.


### Technologies used

The emocha platform runs on a modern Python Tornado-based web stack. Our front-end technologies use Knockout, SASS and Javascript with a variety of Javascript libraries such as Bootstrap for some basic layouts to compliment our custom interfaces, D3 for power dashboard analytics displays, and are compiled using Gulp for deployment. We develop in an Agile setting with daily SCRUM, and verify functionality using Jasmine and Browserstack testkits during our QA cycle.

For PREPme, I built the provider-facing web portal, including patient management and a chatroom, and  built the UI for a cross-platform (iOS and Android) mobile application in Qt QML. The application is currently available for all Maryland residents interested in access to PrEP.

<div class="col-xs-12 col-sm-12 col-md-6">
<h4>PrEPme by emocha</h4>
<iframe src="https://player.vimeo.com/video/208702565" width="100%" height="320px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

### emocha Values Design
Below are assorted screenshots of the PrEPme interface. Working closely with our lead designer, I played a role in conducting user research to create efficient and elegant workflows. My role is principally turning wireframes and mockups into high-fidelity interfaces. Using a modern build system, I lead front-end development using a variety of tools based on CSS3, modern Javascript, and HTML5. All of our apps comply with HIPAA regulatory standards for healthcare privacy.
