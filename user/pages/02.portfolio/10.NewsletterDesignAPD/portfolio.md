---
title: 'Newsletter Publication Design'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# Harvard Art Museum Newsletter Designs
## Concept, design and physical mockups for two issues of the Harvard Art Museum newsletter.

===

<div class="col-xs-12 col-sm-12 col-md-6 embed">
<h3>Design Concept 1</h3>
<iframe style="width:100%; height:500px;" src="//e.issuu.com/embed.html#10295212/6104773" frameborder="0" allowfullscreen></iframe>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 embed">
<h3>Design Concept 2</h3>
<iframe style="width:100%; height:500px;" src="//e.issuu.com/embed.html#10295212/6104777" frameborder="0" allowfullscreen></iframe>
</div>
