---
title: 'Living Walls and Lightboxes'
published: true
summary:
    enabled: true
    format: long
    delimiter: '==='
---

Living Walls are digital light displays custom fabricated for Urbanstems Baltimore. Each 4'x4' wall is illuminated by LED lighting, and built using a pegboard system, allowing flowers or other decorations to be adorned to the wall. These walls were built in various forms for the Baltimore and Philadelphia markets.

The second set of devices are 2'x3' programmable light displays featuring the Urbanstems logomark. These devices are software-controlled, featuring a swappable set of 10 pre-programmed light shows. These boxes are now featured during outreach events in 5 markets nationwide.
