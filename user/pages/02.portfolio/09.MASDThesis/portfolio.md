---
title: 'MA Social Design Thesis: BOOM!'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# MA Social Design Thesis: BOOM!
## BOOM! unleashes the creative and entrepreneurial potential of youth in disinvested communities, exposing them to new technologies and providing the access and skills they need to break down barriers to employment.

===

<div class="col-xs-12 embed">
<h3>Thesis Document</h3>
<iframe style="width:100%; height:500px;" src="//e.issuu.com/embed.html#10295212/45935530" frameborder="0" allowfullscreen></iframe>
</div>
