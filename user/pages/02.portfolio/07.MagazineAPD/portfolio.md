---
title: 'Magazine Publication Design'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# IMPACT> Magazine
## Concept, design and physical mockup for a magazine about social impact.

===

<div class="col-xs-12 embed">
<h3>Design Concept</h3>
<iframe style="width:100%; height:500px;" src="//e.issuu.com/embed.html#10295212/45935473" frameborder="0" allowfullscreen></iframe>
</div>
