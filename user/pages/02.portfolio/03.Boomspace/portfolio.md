---
title: 'Boomspace Makerspace'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
---

# Boomspace Makerspace
## Founding a community-based manufacturing hub in East Baltimore teaching digital manufacturing, programming, and other 21st century digital skills.

===

Boomspace Makerspace was a makerspace designed for community empowerment. In McElderry Park, a neighborhood of East Baltimore breaking out of the effects of institutional racism and generations-long neglect, Boomspace worked with the local neighborhood association and community partners to build a fully-featured digital fabrication space. At its peak, Boomspace engaged with dozens of local youth, teaching the basics of 3D Modeling, 3D Printing, 2D design, Laser cutting, CNC machining, modern web design, game design, and other 21st century digital skills.

As a partnership under the Byrne Criminal Justice Innovation Grant, Boomspace provided both a classroom and a workshop equipped with 14 workstations running Adobe Creative Suite and open source design software, two FDM-based 3D printers, a 20"x24" 40W laser cutter, vinyl and stencil cutter, a small resource library, skills-building class series, and a host of small toys to engage young people in technology and digital design. In partnership with Digit All Systems, Jericho Community Services, and the Julie Community Center, Boomspace was part of a "one-stop" shop for attaining a GED, A+ Certified Computer Technician license, and advanced computer skills.

Read about Boomspace and the Baltimore Maker ecosystem in [EdSurge Magazine](https://www.edsurge.com/news/2015-02-10-meet-maryland-s-magical-makers)

_Boomspace was funded in part by the Maryland Institute College of Art, Robert W. Deutsch Foundation, and the Open-Society Institute-Baltimore_
