---
title: 'emocha Mobile Health Inc'
published: true
summary:
  enabled: true
  format: long
  delimiter: ===
content:
    items:
        '@page.descendants': /portfolio
    order:
        by: default
        dir: desc
    limit: 50
    pagination: false
captions:
    one.png: 'this is the first one'
    two: 'this is the second one'
---

# emocha Mobile Health, Inc.
## Designing and building elegant, functional and revolutionary front-end interfaces.
===

emocha Mobile Health Inc. builds mobile health tools for providers and payers to improve public health outcomes and reduce the high cost of medication non-adherence.

At emocha Mobile Health Inc., I took a role as a multi-disciplinary designer/developer to create mobile health technologies now used around the world. Our technology focuses on closing the gap between patients and providers, to streamline what is called the "continuum of care". emocha is used in the United States, India, Bolivia, Mexico, South Africa, Australia, and other countries connecting over 17,000 patients to medical care, and bettering the outcomes of care for thousands across the globe.

### Technologies used

The emocha platform runs on a modern Python Tornado-based web stack. Our front-end technologies use Knockout, SASS and Javascript with a variety of Javascript libraries such as Bootstrap for some basic layouts to compliment our custom interfaces, D3 for power dashboard analytics displays, and are compiled using Gulp for deployment. We develop in an Agile setting with daily SCRUM, and verify functionality using Jasmine and Browserstack testkits during our QA cycle.

### Impact of emocha Adherence Solutions
Below are two videos **shot, produced and edited by me** explaining the use cases for emocha's innovative products. The first video is a short promotional video for the flagship emocha product for adherence, miDOT. The second video is a longer production featuring two emocha clients explaining the impacts of the emocha technology in their respective health departments in Houston, TX and Sydney, Australia.

miDOT is a mobile application that allows for Directly Observed Therapy (DOT) between a patient and provider team using a mobile app and web interface. DOT is considered the worldwide standard of care for highly infectious diseases, including Tuberculosis. While DOT is highly effective, it is also very expensive and time-consuming. miDOT enables healthcare workers to give patients autonomy and freedom over their care, while using a cost-effective and easy-to-use interface for administering DOT.

<div class="col-xs-12 col-sm-12 col-md-6">
<h4>miDOT by emocha</h4>
<iframe src="https://player.vimeo.com/video/206287619" width="100%" height="320px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

<div class="col-xs-12 col-sm-12 col-md-6">
<h4>Video Directly Observed Therapy</h4>
<iframe src="https://player.vimeo.com/video/208554065" width="100%" height="320px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
<div style="clear: both"></div>

### emocha Values Design
Below are assorted screenshots of the emocha interface. Working closely with our lead designer, I played a role in conducting user research to create efficient and elegant workflows. My role is principally turning wireframes and mockups into high-fidelity interfaces. Using a modern build system, I lead front-end development using a variety of tools based on CSS3, modern Javascript, and HTML5. All of our apps comply with HIPAA regulatory standards for healthcare privacy.
