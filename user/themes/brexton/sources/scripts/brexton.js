$(document).ready(function() {
    //var colors = ['rgb(14, 181, 138)', 'rgb(158, 144, 220)', 'rgb(239, 91, 130)', 'rgb(248, 201, 202)', 'rgb(223, 150, 60)'];
    var colors = ['rgb(9, 33, 64)', 'rgb(2, 73, 89)', 'rgb(242, 199, 119)', 'rgb(242, 71, 56)', 'rgb(191, 42, 42)'];
    var alpha = '0.5';

    // Assign a random background color.
    var color = Math.floor(Math.random() * colors.length);
    $('body.main').css('background-color',  colors[color]);

    var brightness = function(color) {
        var bg_color, rgba, y;
        bg_color = color;
        if ((bg_color != null) && bg_color.length) {
            rgba = bg_color.match(/^rgb(?:a)?\(([0-9]{1,3}),\s([0-9]{1,3}),\s([0-9]{1,3})(?:,\s)?([0-9]{1,3})?\)$/);
            if (rgba != null) {
                if (rgba[4] === '0') {
                    if (this.parent().length) return this.parent().brightness();
                } else {
                    y = 2.99 * rgba[1] + 5.87 * rgba[2] + 1.14 * rgba[3];
                    if (y >= 1275) {
                        return 'light';
                    } else {
                        return 'dark';
                    }
                }
            }
        } else {
            if (this.parent().length) return this.parent().brightness();
        }
    };

    $('body').addClass(brightness(colors[color]));

    $('.body a, .card-container .card-title').on('mouseover', function() {
        $(this).css('color', colors[color]);
    });

    $('.body a, .card-container .card-title').on('mouseleave', function() {
        $(this).css('color', brightness(colors[color]) === 'dark' ? 'white' : 'black');
    });

    var cards = document.getElementsByClassName('card-container');
    for (var i = 0; i < cards.length; i++) {
        var colorWithAlpha = String(colors[color]).replace(')', ',' + alpha + ')');
            colorWithAlpha = colorWithAlpha.replace('rgb', 'rgba');
        $(cards[i]).css('background', 'linear-gradient(' + colorWithAlpha + ',' + colorWithAlpha + '), url(\''+ $(cards[i]).data('img') +'\')');
    }

    // Make a gallery
    $('a.gallery').featherlightGallery();
});
