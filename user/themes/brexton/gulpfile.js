// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    del = require('del'),
    async = require('async'),
    gutil = require('gulp-util'),
    order = require('gulp-order'),
    filter = require('gulp-filter');

gulp.task('styles', function() {
  return gulp.src(['sources/**/*.scss', 'sources/**/*.css'], {base: '.'})
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer('last 2 version'))
    .pipe(concat('brexton.css'))
    .pipe(cssnano())
    .pipe(rename('brexton.min.css'))
    .pipe(gulp.dest('dist/styles'))
});

gulp.task('scripts', function() {
  return gulp.src('sources/**/*.js')
    .pipe(order([
      'vendor/scripts/jquery*.js',
      'vendor/scripts/bootstrap*.js',
      '*.js',
    ]))
    .pipe(concat('brexton.js'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(rename({ suffix: '.min' }))
});

gulp.task('images', function() {
  return gulp.src(['static/img/**/*', '!static/img/icon/**/*'])
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
  return gulp.src('sources/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));
});

// Clean
gulp.task('clean', function() {
  return del(['dist/styles', 'dist/scripts', 'dist/images', 'dist/fonts']);
});

// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('styles', 'scripts', 'fonts', 'images');
});

// Watch
gulp.task('watch', ['default'], function() {
  // Watch .scss files
  gulp.watch(['sources/**/*.scss'], ['styles']);

  gulp.watch('sources/**/*.js', ['scripts']);

  gulp.watch('sources/fonts/**/*', ['fonts']);
});
